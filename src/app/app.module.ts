// Natif modules
import { ServiceWorkerModule } from '@angular/service-worker';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

// Modules
import { CoreModule } from './share/modules/core.module';

// components
import { AppComponent } from './app.component';

// Routing
import { APP_ROUTING } from './app.routing';

// ngrx
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

// others
import { environment } from 'src/environments/environment';
import { reducersMap } from './share/store';
import { AuthEffects } from './share/store/effects/auth.effects';
import { BarEffects } from './share/store/effects/bar.effects';
import { DrinksComponent } from './components/drinks/drinks.component';
import { ToDegreePipe } from './share/pipes/to-degree.pipe';

@NgModule({
  declarations: [AppComponent, ToDegreePipe],
  imports: [
		BrowserModule,
		BrowserAnimationsModule,
		RouterModule.forRoot(APP_ROUTING),
		StoreModule.forRoot(reducersMap),
		EffectsModule.forRoot([AuthEffects, BarEffects]),
		StoreDevtoolsModule.instrument({
			name: 'Ngrx',
			logOnly: environment.production
		}),
		CoreModule,
		ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
