import { PhotosAction, PhotosActionsTypes } from "./photos.actions";
import { Photo } from "../models/photo.model";

export interface PhotosState {
	photos: Photo[];
	filter: string;
}

const defaultPhotoState = {
	photos: [],
	filter: ''
}

export function photosReducer(state: PhotosState = defaultPhotoState, action: PhotosAction): PhotosState {
	switch (action.type) {
		case PhotosActionsTypes.SET_FILTER: {
			return {
				...state,
				filter: action.payload
			};
		}

		case PhotosActionsTypes.FETCH_PHOTOS_SUCCESS: {
			return {
				...state,
				photos: action.payload
			};
		}
	}

	return state;
}