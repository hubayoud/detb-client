import { Action } from "@ngrx/store";
import { Photo } from "../models/photo.model";

export enum PhotosActionsTypes {
	SET_FILTER = '[photos/filter] try set filter',
	FETCH_PHOTOS = '[photos/api] fetch photos',
	FETCH_PHOTOS_SUCCESS = '[photos/api] fetch photos success'
}

export class SetFilter implements Action {
	readonly type = PhotosActionsTypes.SET_FILTER;
	constructor(public payload: string) {}
}

export class FetchPhotos implements Action {
	readonly type = PhotosActionsTypes.FETCH_PHOTOS;
}

export class FetchPhotosSuccess implements Action {
	readonly type = PhotosActionsTypes.FETCH_PHOTOS_SUCCESS;
	constructor(public payload: Photo[]) {}
}

export type PhotosAction = SetFilter | FetchPhotos | FetchPhotosSuccess;