import { createFeatureSelector, createSelector } from "@ngrx/store";
import { PhotosState } from "./photos.reducers";

export const baseSelector = createFeatureSelector('photos');

export const filterPhotosSelector = createSelector(baseSelector, (photosState: PhotosState) => photosState?.filter);

export const photosSelector = createSelector(baseSelector, (photosState: PhotosState) => photosState?.photos);