import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { select, Store } from "@ngrx/store";
import { debounceTime, map, switchMap, take, tap } from "rxjs/operators";

import { State } from "../../../share/store";
import { Photo } from "../models/photo.model";
import { PhotosService } from "../services/photos.service";
import { FetchPhotosSuccess, PhotosActionsTypes } from "./photos.actions";
import { filterPhotosSelector } from './photos.selectors';

@Injectable()
export class PhotosEffect {
	constructor(
		private actions$: Actions,
		private store: Store<State>,
		private photosService: PhotosService
	) {}

	@Effect()
	FetchPhotos$ = this.actions$.pipe(
		ofType(PhotosActionsTypes.FETCH_PHOTOS),
		debounceTime(250),
		switchMap(() => {
			// récupérer le filter depuis l'état du store
			return this.store.pipe(select(filterPhotosSelector), take(1));
		}),
		switchMap((filter: string) => {
			return this.photosService.getPictures(filter);
		}),
		map((photos: Photo[]) => {
			return new FetchPhotosSuccess(photos);
		})
	);
}