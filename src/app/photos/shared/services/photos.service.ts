import { Injectable } from '@angular/core';
import { from } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { createApi } from 'unsplash-js';

@Injectable()
export class PhotosService {
	private unsplash = createApi({
		accessKey: 'eKW8z4wMO_M3uaJsbt4R6x3v09q74kKBtm0R56mLFrw',
	});
	 
  constructor() {}

	public getPictures(filter: string) {
		return from(this.unsplash.search.getPhotos({
			query: filter
		})).pipe(
			map((response: any) => {
				return response?.response?.results.map(res => {
					const smallUrl = res.urls.small;
					if (smallUrl) {
						return { url: smallUrl}
					}
				});
			})
		);

	}
}
