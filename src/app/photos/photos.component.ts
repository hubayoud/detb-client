import { Component, OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { photosSelector } from './shared/store/photos.selectors';
import { Photo } from './shared/models/photo.model';
import { State } from '../share/store';
import { NotificationService } from '../share/services/notification.service';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotosComponent implements OnInit {
	public photos$: Observable<Photo[]>;

  constructor(
		private store: Store<State>,
		private swUpdate: SwUpdate,
		private notificationService: NotificationService
	) {
		this.swUpdate.available.subscribe((version) => {
			if (version) {
				this.swUpdate.activateUpdate().then(() => {
					window.location.reload();
				});
			}
		});

		this.swUpdate.checkForUpdate();
	}

  ngOnInit(): void {
		this.photos$ = this.store.pipe(select(photosSelector));
		this.photos$.subscribe();
	}

	public sendNotification(): void {
		this.notificationService.sendTestNotification();
	}
}
