import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { LayoutModule } from '../share/modules/layout.module';
import { PHOTOS_ROUTES } from './photos.routing';
import { PhotosComponent } from './photos.component';
import { PhotosService } from './shared/services/photos.service';
import { photosReducer } from './shared/store/photos.reducers';
import { PhotosEffect } from './shared/store/photos.effects';



@NgModule({
  declarations: [PhotosComponent],
  imports: [
		LayoutModule,
		RouterModule.forChild(PHOTOS_ROUTES),
		StoreModule.forFeature('photos', photosReducer),
		EffectsModule.forFeature([PhotosEffect])
  ],
	providers: [PhotosService]
})
export class PhotosModule { }
