import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { errorAuthSelector, isLoggedInAuthSelector } from '../../share/store/selectors/auth.selectors';
import { TrySignup } from '../../share/store/actions/auth.actions';
import { matchValues } from '../../share/helpers/match';
import { State } from '../../share/store';
import { subscribeOn, tap } from 'rxjs/operators';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
	public form: FormGroup;
	public err$: Observable<string>;
	public consentChecked = undefined;

	constructor(
		private fb: FormBuilder,
		private router: Router,
		private store: Store<State>
	) { }

  ngOnInit(): void {
		this.form = this.fb.group({
			pseudo: ['', [
				Validators.required,
				Validators.maxLength(20), 
				Validators.minLength(3)
			]],
			email: ['', [
				Validators.required, 
				Validators.email
			]],
			password: ['', [
				Validators.required, 
				Validators.minLength(8)
			]],
			confirmPassword: ['', [
				Validators.required, 
				matchValues('password')
			]],
			consentCGU: [undefined, [
				Validators.required
			]]
		});

		this.form.controls['password'].valueChanges.subscribe(() => {
			this.form.controls['confirmPassword'].updateValueAndValidity();
		});

		this.err$ = this.store.pipe(
			select(errorAuthSelector)
		);
	}
	
	public onSubmit(): void {
		this.store.dispatch(new TrySignup(this.form.value));
	}

	public onCheckboxChange($event: any): void {
		this.consentChecked = $event.checked;
	}
}