import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { currentMenuSelector } from '../../share/store/selectors/bar.selectors';
import { FetchBarMenu } from '../../share/store/actions/bar.actions';
import { Menu } from '../../share/models/menu.model';
import { State } from '../../share/store';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-drinks',
  templateUrl: './drinks.component.html',
  styleUrls: ['./drinks.component.css']
})
export class DrinksComponent implements OnInit {
	public menu$: Observable<Menu> = this.store.select(currentMenuSelector);
	public tests: number[] = [0, 1, 2, 3, 4];

  constructor(
		private activatedRoute: ActivatedRoute,
		private store: Store<State>,
	) {}

  ngOnInit(): void {
		this.activatedRoute.params.subscribe(params => {
			const barId = params['id'];

			if (barId) {
				this.store.dispatch(new FetchBarMenu(barId));
			}
		});

	}
}
