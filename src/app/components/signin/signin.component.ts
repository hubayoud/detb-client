import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { State } from '../../share/store';
import { TrySignin } from '../../share/store/actions/auth.actions';
import { errorAuthSelector, isLoggedInAuthSelector } from '../../share/store/selectors/auth.selectors';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
	public form: FormGroup;
	public infoMessage = '';
	
	public error$: Observable<string>;

  constructor(
		private fb: FormBuilder,
		private router: Router,
		private route: ActivatedRoute,
		private store: Store<State>,
	) { }

  ngOnInit(): void {
		this.form = this.fb.group({
			email: ['', [Validators.required, Validators.email]],
			password: ['', Validators.required]
		});

		this.error$ = this.store.pipe(
			select(errorAuthSelector)
		);

		this.route.queryParams
		.subscribe(params => {
			if(params.registered !== undefined && params.registered === 'true') {
				this.infoMessage = 'Registration Successful! Please Login!';
			}
		});
	}
	
	public onSubmit(): void {
		this.store.dispatch(new TrySignin(this.form.value));
	}
}
