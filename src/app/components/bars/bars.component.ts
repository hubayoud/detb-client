import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Bar } from '../../share/models/bar.model';
import { Store } from '@ngrx/store';
import { State } from '../../share/store';
import { allBarsSelector } from '../../share/store/selectors/bar.selectors';
import { FetchBars } from 'src/app/share/store/actions/bar.actions';

@Component({
  selector: 'app-bars',
  templateUrl: './bars.component.html',
  styleUrls: ['./bars.component.css']
})
export class BarsComponent implements OnInit {
	public bars$: Observable<Bar[]> = this.store.select(allBarsSelector);

  constructor(
		private store: Store<State>,
	) {}

  ngOnInit(): void {
		this.store.dispatch(new FetchBars());
	}
}
