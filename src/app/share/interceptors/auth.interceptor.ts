import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { select, Store } from "@ngrx/store";
import { Observable } from "rxjs";

import { State } from "../store";
import { tokenAuthSelector } from "../store/selectors/auth.selectors";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
	private token: string;
	
	constructor(private store: Store<State>) {
		this.store.pipe(
			select(tokenAuthSelector)
		).subscribe((token: string) => {
			this.token = token;
		});
	}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		if (this.token) {
			// si il y a un token, on crée une nouvelle req en clonant l'ancienne et en modifiant le header
			const authReq = req.clone({
				headers: req.headers.set('authorization', this.token)
			});
			return next.handle(authReq);
		} else {
			// si il y a pas de token, on modifie rien dans la requete, on la laisse passer
			return next.handle(req);
		}
	}
}
