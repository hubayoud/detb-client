import { Injectable } from '@angular/core';
import { 
	CanActivate, 
	ActivatedRouteSnapshot, 
	RouterStateSnapshot, 
	Router
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { isLoggedInAuthSelector } from '../store/selectors/auth.selectors';
import { State } from '../store';

@Injectable({
  providedIn: 'root'
})
export class AlreadyLoggedInGuard implements CanActivate {
	constructor(
		private store: Store<State>,
		private router: Router
	) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		return this.store.select(isLoggedInAuthSelector).pipe(
			take(1),
			map((isLoggedIn: boolean) => {
				if (isLoggedIn) {
					this.router.navigate(['/profile']);
				}

				return !isLoggedIn;
			})
		);
  }
}
