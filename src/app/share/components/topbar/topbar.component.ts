import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { State } from '../../store';
import { Logout } from '../../store/actions/auth.actions';
import { isLoggedInAuthSelector } from '../../store/selectors/auth.selectors';
import { FetchPhotos, SetFilter } from 'src/app/photos/shared/store/photos.actions';
import { TryFetchCurrentUser } from '../../store/actions/auth.actions';
import { userAuthSelector } from '../../store/selectors/auth.selectors';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {
	public isLoggedIn$: Observable<boolean>;
	public isMenuOpen: boolean = false;

	public currentUser$: Observable<User>;
	
  constructor(
		private store: Store<State>,
	) {}

  ngOnInit(): void {
		this.isLoggedIn$ = this.store.pipe(
			select(isLoggedInAuthSelector)
		);
		this.currentUser$ = this.store.pipe(select(userAuthSelector));
		this.store.dispatch(new TryFetchCurrentUser());
	}

	public onLogout(): void {
		this.store.dispatch(new Logout());
	}

	public onSidenavClick(): void {
		this.isMenuOpen = false;
	  }
}
