import { createFeatureSelector, createSelector } from "@ngrx/store";
import { AuthState } from "../reducers/auth.reducers";

export const authSelector = createFeatureSelector('auth');

export const errorAuthSelector = createSelector(authSelector, (authState: AuthState) => authState?.error);

export const tokenAuthSelector = createSelector(authSelector, (authState: AuthState) => authState?.token);

export const isLoggedInAuthSelector = createSelector(authSelector, (authState: AuthState) => authState?.isLoggedIn);

export const userAuthSelector = createSelector(authSelector, (authState: AuthState) => authState?.user);