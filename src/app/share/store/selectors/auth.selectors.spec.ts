import * as AuthSelectors from './auth.selectors';

describe('auth selectors', () => {
	describe('errorAuthSelector', () => {
		it('should return null (no error)', () => {
			const mockState = {
				auth: {
					user: null,
					token: null,
					isLoggedIn: null,
					error: null
				}
			};

			expect(AuthSelectors.errorAuthSelector(mockState)).toEqual(null);
		});

		it('should return error', () => {
			const mockState = {
				auth: {
					user: null,
					token: null,
					isLoggedIn: null,
					error: 'error'
				}
			};

			expect(AuthSelectors.errorAuthSelector(mockState)).toEqual('error');
		});
	});
});