import { createFeatureSelector, createSelector } from "@ngrx/store";
import { BarState } from "../reducers/bar.reducers";

export const baseSelector = createFeatureSelector('bars');

export const allBarsSelector = createSelector(baseSelector, (barState: BarState) => barState?.bars);

export const currentMenuSelector = createSelector(baseSelector, (barState: BarState) => barState?.currentMenu);

export const currentBarIdSelector = createSelector(baseSelector, (barState: BarState) => barState?.currentBarId);