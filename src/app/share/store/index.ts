import { ActionReducerMap } from "@ngrx/store";
import { authReducer, AuthState } from "./reducers/auth.reducers";
import { barReducer, BarState } from "./reducers/bar.reducers";

export interface State {
	auth: AuthState;
	bars: BarState;
}

export const reducersMap: ActionReducerMap<State> = {
	auth: authReducer,
	bars: barReducer
};