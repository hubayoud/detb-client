import { Bar } from "../../models/bar.model";
import { Menu } from "../../models/menu.model";
import { BarsActions, BarActionsTypes } from "../actions/bar.actions";

export interface BarState {
	bars: Bar[];
	currentMenu: Menu;
	currentBarId: string;
	error: string;
}

const defaultBarState = {
	bars: [],
	currentMenu: null,
	currentBarId: '',
	error: '',
}

export function barReducer(state: BarState = defaultBarState, action: BarsActions): BarState {
	switch (action.type) {

		case BarActionsTypes.FETCH_BAR_MENU: {
			return {
				...state,
				currentBarId: action.payload
			};
		}

		case BarActionsTypes.FETCH_BARS_SUCCESS: {
			return {
				...state,
				bars: action.payload
			};
		}

		case BarActionsTypes.FETCH_BAR_MENU_SUCCESS: {
			return {
				...state,
				currentMenu: action.payload
			};
		}

		case BarActionsTypes.FETCH_BAR_MENU_FAILED:
		case BarActionsTypes.FETCH_BARS_FAILED: {
			return {
				...state,
				error: action.payload
			};
		}
	}

	return state;
}