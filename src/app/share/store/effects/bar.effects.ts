import { Actions, createEffect, ofType } from "@ngrx/effects";
import { Injectable } from "@angular/core";
import { catchError, map, mergeMap, withLatestFrom } from "rxjs/operators";
import { EMPTY, of } from "rxjs";

import { BarActionsTypes, FetchBarMenuFailed, FetchBarMenuSuccess, FetchBarsFailed, FetchBarsSuccess } from "../actions/bar.actions";
import { BarService } from "../../services/bar.service";
import { Bar } from "../../models/bar.model";
import { Menu } from "../../models/menu.model";
import { Store } from "@ngrx/store";
import { State } from "..";
import { currentBarIdSelector } from "../selectors/bar.selectors";

@Injectable()
export class BarEffects {
	constructor(
		private actions$: Actions,
		private barService: BarService,
		private store: Store<State>
	) {}

	loadBars$ = createEffect(() => this.actions$.pipe(
		ofType(BarActionsTypes.FETCH_BARS),
		mergeMap(() => this.barService.getAll()
			.pipe(
				map((bars: Bar[]) => (new FetchBarsSuccess(bars))),
				catchError(() => of(new FetchBarsFailed('failed fetching all bars')))
			))
	));

	loadMenu$ = createEffect(() => this.actions$.pipe(
		ofType(BarActionsTypes.FETCH_BAR_MENU),
		withLatestFrom(this.store.select(currentBarIdSelector)),
		mergeMap(([_, barId]) => this.barService.getMenu(barId)
			.pipe(
				map((menu: Menu) => (new FetchBarMenuSuccess(menu))),
				catchError(() => of(new FetchBarMenuFailed(`failed fetch bar's menu`)))
			))
	));
}