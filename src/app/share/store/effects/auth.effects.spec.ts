import { HttpClientModule } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { StoreModule } from "@ngrx/store";
import { Observable, of } from "rxjs";
import { hot, cold } from 'jasmine-marbles';

import * as AuthActions from '../actions/auth.actions';
import { AuthService } from "../../services/auth.service";
import { UserService } from "../../services/user.service";
import { AuthEffects } from "./auth.effects";
import { User } from "../../models/user.model";
import { Router } from "@angular/router";

describe('auth effect', () => {
	let actions: Observable<any>;
	let effects: AuthEffects;
	let routerService: Router;
	let authService: AuthService;

	const user: User = {
		email: 'test@test.fr',
		pseudo: 'testName',
		password: 'testtest'
	};
	
	beforeAll(() => {

		TestBed.configureTestingModule({
			imports: [
				HttpClientModule,
				StoreModule.forRoot({}),
				RouterTestingModule.withRoutes([{path: 'signin', component: {} as any}])
			],
			providers: [
				AuthEffects,
				provideMockActions(() => actions),
				UserService,
				AuthService,
			],
		});

		effects = TestBed.inject(AuthEffects);
		authService = TestBed.inject(AuthService);
		routerService = TestBed.inject(Router);
	});

	describe('trySignUp$ effect', () => {
		it('should return SignupSuccess action', () => {
			spyOn(authService, 'signup').and.returnValue(of(user));
			// chaque tiret - correspond à 1ms puis on émet une nouvelle valeur (ici un try signup action)
			actions = hot('---a-', { a: new AuthActions.TrySignup(user) });
			const expected = cold('---b', { b: new AuthActions.SignupSuccess(user) });
	
			expect(effects.trySignUp$).toBeObservable(expected);
		});

		it('should return error', () => {	
			spyOn(authService, 'signup').and.returnValue(cold('-#', {}, 'error'));
			actions = hot('---a-', { a: new AuthActions.TrySignup(user)});
			const expected = cold('----b', { b: new AuthActions.SignupError('error') });
	
			expect(effects.trySignUp$).toBeObservable(expected);
		});
	});
	
	describe('signupSuccess$ effect', () => {
		it('should navigate to /signin from signupSuccess$', () => {
			spyOn(routerService, 'navigate').and.callThrough();
			actions = hot('---a-', { a: new AuthActions.SignupSuccess(user)});
			const expected = cold('---b', { b: new AuthActions.SignupSuccess(user)});

			expect(effects.signupSuccess$).toBeObservable(expected);
			
			effects.signupSuccess$.subscribe(() => {
				expect(routerService.navigate).toHaveBeenCalledWith(['/signin']);
			});
		});
	});

});