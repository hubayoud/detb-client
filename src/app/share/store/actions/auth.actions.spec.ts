import * as AuthActions from './auth.actions';

describe('auth actions', () => {
	describe('try signup action', () => {
		it('should create a try signup action', () => {
			const payload = {
				email: 'test@test.fr',
				pseudo: 'pseudoTest',
				password: 'testtest'
			};
			const action = new AuthActions.TrySignup(payload);

			// on déconstruit action pour que ce soit un object javascript pour pouvoir le comparer
			expect({ ...action }).toEqual({
				type: AuthActions.AuthActionsTypes.TRY_SIGNUP,
				payload: payload
			});
		});
	});

	describe('signup error action', () => {
		it('should create a signup error action', () => {
			const payload = {
				message: 'error message'
			};

			const action = new AuthActions.SignupError(payload);

			expect({ ...action }).toEqual({
				type: AuthActions.AuthActionsTypes.SIGNUP_ERROR,
				payload: payload
			});
		});
	});
});