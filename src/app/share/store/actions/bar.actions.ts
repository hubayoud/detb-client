import { Action } from "@ngrx/store";
import { Bar } from "../../models/bar.model";
import { Menu } from "../../models/menu.model";

export enum BarActionsTypes {
	FETCH_BARS = '[bars/api] fetch all bars',
	FETCH_BARS_SUCCESS = '[bars/api] fetch bars success',
	FETCH_BARS_FAILED = '[bars/api] fetch bars failed',
	FETCH_BAR_MENU = '[bars/menu/api] fetch bar menu',
	FETCH_BAR_MENU_SUCCESS = '[bar/menu/api] fetch bar menu success',
	FETCH_BAR_MENU_FAILED = '[bars/menu/api] fetch bar menu failed',
}

export class FetchBars implements Action {
	readonly type = BarActionsTypes.FETCH_BARS;
}

export class FetchBarsSuccess implements Action {
	readonly type = BarActionsTypes.FETCH_BARS_SUCCESS;
	constructor(public payload: Bar[]) {}
}

export class FetchBarsFailed implements Action {
	readonly type = BarActionsTypes.FETCH_BARS_FAILED;
	constructor(public payload: string) {}
}

export class FetchBarMenu implements Action {
	readonly type = BarActionsTypes.FETCH_BAR_MENU;
	constructor(public payload: string) {}
}

export class FetchBarMenuSuccess implements Action {
	readonly type = BarActionsTypes.FETCH_BAR_MENU_SUCCESS;
	constructor(public payload: Menu) {}
}

export class FetchBarMenuFailed implements Action {
	readonly type = BarActionsTypes.FETCH_BAR_MENU_FAILED;
	constructor(public payload: string) {}
}

export type BarsActions = FetchBars |
 	FetchBarsSuccess |
 	FetchBarsFailed |
	FetchBarMenu | 
	FetchBarMenuSuccess | 
	FetchBarMenuFailed;