import { Action } from "@ngrx/store";
import { User } from "../../models/user.model";

export enum AuthActionsTypes {
	TRY_SIGNUP = '[auth/user] try signup',
	SIGNUP_SUCCESS = '[auth/user] signup success',
	SIGNUP_ERROR = '[auth/user] signup error',
	TRY_SIGNIN = '[auth/user] try signin',
	SIGNIN_SUCCESS = '[auth/user] signin success',
	SIGNIN_ERROR = '[auth/user] signin error',
	LOGOUT = '[auth/user] user logout',
	TRY_FETCH_CURRENT_USER = '[auth/user] try fetch current user',
	SET_CURRENT_USER = '[auth/user] set current user',
	TRY_REFRESH_TOKEN = '[auth/token] try refresh token'
}

export class TrySignup implements Action {
	readonly type = AuthActionsTypes.TRY_SIGNUP;
	constructor(public payload: User) {}
}

export class SignupSuccess implements Action {
	readonly type = AuthActionsTypes.SIGNUP_SUCCESS;
	constructor(public payload: User) {}
}

export class SignupError implements Action {
	readonly type = AuthActionsTypes.SIGNUP_ERROR;
	constructor(public payload: any) {}
}

export class TrySignin implements Action {
	readonly type = AuthActionsTypes.TRY_SIGNIN;
	constructor(public payload: { email: string, password: string}) {}
}

export class SigninSuccess implements Action {
	readonly type = AuthActionsTypes.SIGNIN_SUCCESS;
	constructor(public payload: string) {}
}

export class SigninError implements Action {
	readonly type = AuthActionsTypes.SIGNIN_ERROR;
	constructor(public payload: any) {}
}

export class TryFetchCurrentUser implements Action {
	readonly type = AuthActionsTypes.TRY_FETCH_CURRENT_USER;
}

export class SetCurrentUser implements Action {
	readonly type = AuthActionsTypes.SET_CURRENT_USER;
	constructor(public payload: User) {}
}

export class TryRefreshToken implements Action {
	readonly type = AuthActionsTypes.TRY_REFRESH_TOKEN;
}

export class Logout implements Action {
	readonly type = AuthActionsTypes.LOGOUT;
}

export type AuthActions = TrySignup |
													SignupSuccess |
													SignupError |
													TrySignin |
													SigninSuccess |
													SigninError |
													TryFetchCurrentUser |
													TryRefreshToken |
													Logout |
													SetCurrentUser;