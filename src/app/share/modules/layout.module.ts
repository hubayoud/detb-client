import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MaterialModule } from './material.module';

const MODULES = [
	FlexLayoutModule,
	CommonModule,
	MaterialModule
];

@NgModule({
  imports: [...MODULES],
	exports: [...MODULES]
})
export class LayoutModule { }
