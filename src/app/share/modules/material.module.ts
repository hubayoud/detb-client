import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSidenavModule} from '@angular/material/sidenav';

const MODULES = [
	MatToolbarModule,
	MatButtonModule,
	MatInputModule,
	MatFormFieldModule,
	MatCardModule,
	MatIconModule,
	MatCheckboxModule,
	MatSidenavModule,
];

@NgModule({
  imports: [...MODULES],
	exports: [...MODULES]
})
export class MaterialModule { }
