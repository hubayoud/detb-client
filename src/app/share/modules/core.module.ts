// Natif modules
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

// Modules
import { LayoutModule } from './layout.module';

// Services
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';

// Guards
import { AuthGuard } from '../guards/auth.guard';

// Components
import { SignupComponent } from '../../components/signup/signup.component';
import { SigninComponent } from '../../components/signin/signin.component';
import { TopbarComponent } from '../components/topbar/topbar.component';
import { WelcomeComponent } from '../../components/welcome/welcome.component';
import { BarsComponent } from '../../components/bars/bars.component';
import { DrinksComponent } from '../../components/drinks/drinks.component';

// Interceptors
import { AuthInterceptor } from '../interceptors/auth.interceptor';

const COMPONENTS = [
	SignupComponent,
	SigninComponent,
	WelcomeComponent,
	BarsComponent,
	TopbarComponent,
	DrinksComponent
];

@NgModule({
  imports: [
		HttpClientModule,
		LayoutModule,
		ReactiveFormsModule,
		RouterModule
	],
	declarations: [...COMPONENTS],
	exports: [...COMPONENTS],
	providers: [
		AuthService,
		UserService,
		AuthGuard,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true,
		}
	]
})
export class CoreModule { }
