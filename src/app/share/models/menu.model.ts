import { Bar } from "./bar.model";
import { Drink } from "./drink.model";

export interface Menu {
	bar: Bar;
	drinks: Drink[];
}