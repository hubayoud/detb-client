import { User } from "./user.model";

export interface JWToken {
	isAuthenticated: boolean;
	token: string;
	user?: User;
}