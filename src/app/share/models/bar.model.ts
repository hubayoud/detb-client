export interface Bar {
	_id: string;
	shortname: string;
	longname: string;
	description: string;
	adress: string;
	logo: string;
}