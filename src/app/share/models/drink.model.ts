export interface Drink {
	id: number,
	shortname: string,
	longname: string,
	description: string,
	idFamilyDrink: number,
	abv: string,
	img: string,
	sizes: number[],
}