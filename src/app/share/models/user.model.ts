export interface User {
	email: string;
	pseudo: string;
	password: string;
	consentCGU: boolean;
}