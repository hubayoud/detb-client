import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toDegree'
})
export class ToDegreePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): number {

    return (+value) * 100;
  }

}
