import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Menu } from '../models/menu.model';
import { Bar } from '../models/bar.model';

@Injectable({
  providedIn: 'root'
})
export class BarService {

  constructor(private http: HttpClient) {}

	// authInterceptor add the token in the HTTP request
	public getAll(): Observable<Bar[]> {
		return this.http.get<Bar[]>('/api/v1/bars');
	}

	public getMenu(barId: string): Observable<Menu> {
		return this.http.get<Menu>(`/api/v1/menu/${barId}`)
	}
}
