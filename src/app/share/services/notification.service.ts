import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SwPush } from '@angular/service-worker';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
	private readonly publicKey = "BClcIn39PpLhEUqQMR-6fvOWyxLfoLr2evEhKg9D9U8HnjKGR9e9bd-uXMBVra1WeenNn0Aa9unlngRCe90Kae0";
  
	constructor(
		private swPush: SwPush,
		private http: HttpClient
	) {}

	public offerNotifications(): void {
		const objectKey = {
			serverPublicKey: this.publicKey
		};

		this.swPush.requestSubscription(objectKey)
		.then((sub: PushSubscription) => {
			console.log(`l'utilisateur a accepté les notifications`, sub);
		
			this.http.post(`/api/v1/notifications`, sub)
			.subscribe(
				() => {
					console.log(`La souscription a bien été enregistrée pour cet utilisateur`);
				},(err) => {
					console.log(`Le sub n'a pas pu être sauvegardé`, err);
				}
			);
		})
		.catch(() => {
			console.log(`l'utilisateur a refusé les notifications`);
		})
	}

	public sendTestNotification(): void {
		this.http.get('/api/v1/notifications/test').subscribe();
	}
}
