import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Store } from '@ngrx/store';

import { BehaviorSubject, Observable, timer } from 'rxjs';
import { tap } from 'rxjs/operators';

import { TryRefreshToken } from '../store/actions/auth.actions';
import { JWToken } from '../models/JWToken.model';
import { User } from '../models/user.model';
import { State } from '../store';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
	public JWToken: BehaviorSubject<JWToken> = new BehaviorSubject({
		isAuthenticated: null,
		token: null
	});

	constructor(
		private http: HttpClient,
		private store: Store<State>
	) {
		this.initToken();
	}

	private initToken(): void {
		const token = localStorage.getItem('jwt');

		if (token) {
			this.JWToken.next({
				isAuthenticated: true,
				token: token
			});
		} else {
			this.JWToken.next({
				isAuthenticated: false,
				token: null
			});
		}
	}

	public initRefreshToken() {
		return timer(5000, 10000).pipe(
			tap(() => this.store.dispatch(new TryRefreshToken()))
		);
	}

	public refreshToken(): Observable<string> {
		return this.http.get<string>('/api/v1/auth/refresh_token');
	}
	
	public signup(user: User): Observable<User> {
		return this.http.post<User>('/api/v1/auth/signup', user);
	}

	public signin(credentials: { email: string, password: string}): Observable<string> {
		return this.http.post<string>('/api/v1/auth/signin', credentials);
	}
}
