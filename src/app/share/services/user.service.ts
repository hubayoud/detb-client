import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
	constructor(private http: HttpClient) {}
	
	// on crée un interceptor pour ajouter le token dans la requete HTTP
	public getCurrentUser(): Observable<User> {
		return this.http.get<User>('/api/v1/user/current');
	}
}
