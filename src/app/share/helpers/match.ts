import { AbstractControl, ValidationErrors } from "@angular/forms";

export function matchValues(matchTo: string): (AbstractControl) => ValidationErrors | null {
	return (control: AbstractControl): ValidationErrors | null => {
		if (!!control.parent?.value) {
			const controlValue = control.value;
			const matchToValue = control.parent?.controls[matchTo]?.value;

			return (controlValue === matchToValue) ? null : { noMatch: true };
		}
	}
};