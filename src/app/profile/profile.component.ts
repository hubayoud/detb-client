import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { User } from '../share/models/user.model';
import { State } from '../share/store';
import { TryFetchCurrentUser } from '../share/store/actions/auth.actions';
import { userAuthSelector } from '../share/store/selectors/auth.selectors';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
	public currentUser$: Observable<User>;

  constructor(private store: Store<State>) {}

  ngOnInit(): void {
		this.currentUser$ = this.store.pipe(select(userAuthSelector));
		this.store.dispatch(new TryFetchCurrentUser());
	}

}
