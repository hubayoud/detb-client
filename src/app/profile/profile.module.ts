import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProfileComponent } from './profile.component';
import { PROFILE_ROUTING } from './profile.routing';
import { LayoutModule } from '../share/modules/layout.module';

@NgModule({
	imports: [
		RouterModule.forChild(PROFILE_ROUTING),
		LayoutModule
	],
  declarations: [
		ProfileComponent
	]
})
export class ProfileModule { }
