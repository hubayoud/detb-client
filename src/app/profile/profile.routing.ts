import { Route } from "@angular/router";
import { ProfileComponent } from "./profile.component";

export const PROFILE_ROUTING: Route[] = [
	{ path: '', component: ProfileComponent }
];