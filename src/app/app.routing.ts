import { Route } from "@angular/router";
import { BarsComponent } from "./components/bars/bars.component";
import { DrinksComponent } from "./components/drinks/drinks.component";
import { SigninComponent } from "./components/signin/signin.component";
import { SignupComponent } from "./components/signup/signup.component";
import { WelcomeComponent } from "./components/welcome/welcome.component";
import { AlreadyLoggedInGuard } from "./share/guards/already-logged-in.guard";
import { AuthGuard } from "./share/guards/auth.guard";

export const APP_ROUTING: Route[] = [
	{ path: 'welcome', canActivate: [AlreadyLoggedInGuard], component: WelcomeComponent },
	{ path: 'photos', canActivate: [AuthGuard], loadChildren: () => import('./photos/photos.module').then(m => m.PhotosModule) },
	{ path: 'profile', canActivate: [AuthGuard], loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule) },
	{ path: 'bars', canActivate: [AuthGuard], component: BarsComponent },
	{ path: 'menu/:id', canActivate: [AuthGuard], component: DrinksComponent },
	{ path: 'signup', canActivate: [AlreadyLoggedInGuard], component: SignupComponent },
	{ path: 'signin', canActivate: [AlreadyLoggedInGuard], component: SigninComponent },
	{ path: '', redirectTo: '', pathMatch: 'full' },
	{ path: '**', redirectTo: '', pathMatch: 'full' },
];